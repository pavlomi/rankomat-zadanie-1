# PROBLEM
```
Zadanie polega na stworzeniu programu transformującego dane wejściowe (z pliku test1.xlsx) do
listy obiektów typu Node(id: Int, name: String, nodes: List[Node]) i wypisaniu tej listy na standardowe wyjście 
w formacie JSON. Elementów w liście będzie 4, pierwszy (A) będzie miał dwa podrzędne i tak dalej.
```

## Run
```
sbt run
```

## Run Tests
```sbtshell
sbt test
```

import sbt._

object Versions {
  val scalaTestV = "3.0.5"
  val sprayJsonV = "1.3.5"
  val apacheXml  = "3.9"
  val configV    = "1.3.2"
}

object Dependencies {
  import Versions._

  lazy val test = Seq(
    "org.scalatest" %% "scalatest" % scalaTestV % "test"
  )

  lazy val others = Seq(
    "com.typesafe"   % "config"      % configV,
    "io.spray"       %% "spray-json" % sprayJsonV,
    "org.apache.poi" % "poi"         % apacheXml,
    "org.apache.poi" % "poi-ooxml"   % apacheXml
  )

  lazy val dependencies = test ++ others
}

package rankomat.xmlparser.entity
import rankomat.xmlparser.dto.Mapping

case class Tree(rootNodes: Seq[Node] = Seq.empty[Node]) {
  def addNode(mapping: Mapping): Tree = mapping.getParentName match {
    case Some(_) => Tree(rootNodes.map(_.addChild(mapping)))
    case None    => Tree(rootNodes :+ Node.from(mapping))
  }

  def flatten: Seq[Node] = rootNodes
}

object Tree {
  def buildTree(mapping: Seq[Mapping]): Tree = mapping.foldLeft(Tree())(_.addNode(_))
}

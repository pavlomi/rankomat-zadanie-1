package rankomat.xmlparser
import rankomat.xmlparser.entity.Tree
import rankomat.xmlparser.json.JsonProtocol

object RankomatPresenter extends JsonProtocol {
  import spray.json._

  def show(tree: Tree): String = tree.flatten.toJson.toString
}

package rankomat.xmlparser.json
import rankomat.xmlparser.entity.Node
import spray.json.{DefaultJsonProtocol, JsonFormat}

trait JsonProtocol extends DefaultJsonProtocol {
  implicit lazy val personFormat: JsonFormat[Node] = lazyFormat(jsonFormat3(Node.apply))
}

package rankomat.xmlparser
import com.typesafe.config.{Config, ConfigFactory}
import rankomat.xmlparser.config.ParserConfig
import rankomat.xmlparser.entity.Tree

object Boot extends App {

  // bootstrap
  lazy val config: Config = ConfigFactory.load()

  val parserConfig  = ParserConfig(config.getString("rankomat.file-name"))
  lazy val mappings = RankomatXMLParser(parserConfig).parse
  lazy val tree     = Tree.buildTree(mappings)
  println(RankomatPresenter.show(tree))
}

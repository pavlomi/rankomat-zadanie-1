package rankomat.xmlparser
import org.scalatest.FlatSpec
import rankomat.xmlparser.entity.{Node, Tree}

class RankomatPresenterSpec extends FlatSpec {

  "show" should "return json string of empty tree" in {
    val tree = Tree()

    assert(RankomatPresenter.show(tree) == "[]")
  }

  "show" should "return json string of nonempty tree" in {
    val node = Node(1, "A", List.empty)
    val tree = Tree(Seq(node))

    val json =
      """
        | [
        |   {
        |     "id": 1,
        |     "name": "A",
        |     "nodes": []
        |   }
        | ]
      """.stripMargin.filterNot(_.isWhitespace)
    assert(RankomatPresenter.show(tree) == json)
  }
}

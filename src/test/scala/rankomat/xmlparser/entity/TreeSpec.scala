package rankomat.xmlparser.entity
import org.scalatest.FlatSpec
import rankomat.xmlparser.dto.Mapping

class TreeSpec extends FlatSpec {
  val random = scala.util.Random

  "addNode" should "return tree with new node" in {
    val tree    = Tree()
    val mapping = Mapping(random.nextInt(), "A")

    assert(tree.addNode(mapping) == tree.copy(rootNodes = Seq(Node(mapping.id, mapping.name, List.empty))))
  }

  "addNode" should "return tree with new subnode" in {
    val node    = Node(random.nextInt(), "A", List.empty)
    val tree    = Tree(Seq(node))
    val mapping = Mapping(random.nextInt, "AA")

    assert(tree.addNode(mapping) == tree.copy(rootNodes = Seq(node.copy(nodes = List(Node(mapping.id, mapping.name, List.empty))))))
  }

  "buildTree" should "return new tree" in {
    val mapping = Mapping(random.nextInt, "A")

    val newTree = Tree.buildTree(Seq(mapping))

    assert(newTree == Tree(Seq(Node(mapping.id, mapping.name, List.empty))))
  }

  "buildTree" should "return new empty tree" in {

    val newTree = Tree.buildTree(Seq.empty)

    assert(newTree == Tree())
  }
}

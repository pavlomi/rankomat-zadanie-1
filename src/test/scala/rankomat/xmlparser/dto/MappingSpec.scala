package rankomat.xmlparser.dto
import org.scalatest.FlatSpec

class MappingSpec extends FlatSpec {

  val random = scala.util.Random

  "getParentName" should "return None if name is one symbol string" in {
    val mapping = Mapping(random.nextInt(), "A")
    assert(mapping.getParentName.isEmpty)
  }

  "getParentName" should "return Some(_) if name is two symbol string" in {
    val mapping = Mapping(random.nextInt(), "AA")
    assert(mapping.getParentName == Some("A"))
  }

}
